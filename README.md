Movie Catalogue adalah aplikasi untuk melihat daftar film.

[Download APK](https://gitlab.com/zainfikrih/moview-omdb/-/raw/master/assets/app.apk?inline=false)

## Library

### 1. Language
- Kotlin

### 2. Design Pattern and Architecture
- MVVM Design Pattern

### 3. Networking and Support
- Retrofit
- Coroutines
- OkHTTP
- Moshi

### 4. View Support
- AndroidX
- Ktx
- Glide
- ViewBinding

### 5. Dependency Injection
- Koin

### 6. Jetpack
- ViewModel
- LiveData
- Android KTX
- MultiDex
- AppCompat


<img src="/assets/1.jpeg" width="400"/>
<img src="/assets/2.jpeg" width="400"/>

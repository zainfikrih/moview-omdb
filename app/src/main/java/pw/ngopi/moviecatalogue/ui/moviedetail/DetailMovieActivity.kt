package pw.ngopi.moviecatalogue.ui.moviedetail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast
import org.koin.androidx.viewmodel.ext.android.viewModel
import pw.ngopi.moviecatalogue.databinding.ActivityDetailMovieBinding
import pw.ngopi.moviecatalogue.utils.Status
import pw.ngopi.moviecatalogue.viewmodel.MovieViewModel

class DetailMovieActivity : AppCompatActivity() {

    companion object {
        const val ARGS_DETAIL_MOVIE = "id"
    }

    private val movieViewModel: MovieViewModel by viewModel()
    private lateinit var binding: ActivityDetailMovieBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        binding = ActivityDetailMovieBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val id = intent.getStringExtra(ARGS_DETAIL_MOVIE)
        id?.let { movieViewModel.getMovieDetail(it) }

        movieViewModel.movieDetailData.observe(this, {
            when (it.status) {
                Status.LOADING -> {
                }
                Status.SUCCESS -> {
                    it.data?.let { movie ->
                        with(binding) {
                            detailsCollapsingToolbar.title = movie.title
                            Glide.with(this@DetailMovieActivity)
                                .load(movie.poster)
                                .into(detailsImgToolbar)
                            tvGenre.text = movie.genre
                            tvYear.text = movie.year
                            tvPlot.text = movie.plot
                            btnFav.onClick {
                                toast("Coming Soon...")
                            }
                        }
                    }
                }
                else -> {
                    onBackPressed()
                }
            }
        })
    }
}
package pw.ngopi.moviecatalogue.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel
import pw.ngopi.moviecatalogue.databinding.ActivityMainBinding
import pw.ngopi.moviecatalogue.ui.adapter.MovieAdapter
import pw.ngopi.moviecatalogue.ui.moviedetail.DetailMovieActivity
import pw.ngopi.moviecatalogue.ui.moviedetail.DetailMovieActivity.Companion.ARGS_DETAIL_MOVIE
import pw.ngopi.moviecatalogue.utils.Status
import pw.ngopi.moviecatalogue.utils.gone
import pw.ngopi.moviecatalogue.utils.visible
import pw.ngopi.moviecatalogue.viewmodel.MovieViewModel

class MainActivity : AppCompatActivity() {

    private val movieViewModel: MovieViewModel by viewModel()
    private lateinit var binding: ActivityMainBinding
    private lateinit var movieAdapter: MovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initAdapter()
        movieViewModel.getMovies()

        movieViewModel.movieData.observe(this, {
            when (it.status) {
                Status.LOADING -> binding.pbMovie.visible()
                Status.SUCCESS -> {
                    binding.pbMovie.gone()
                    movieAdapter.setData(it.data?.search)
                }
                else -> {
                    binding.pbMovie.gone()
                }
            }
        })
    }

    private fun initAdapter() {
        movieAdapter = MovieAdapter {
            startActivity<DetailMovieActivity>(ARGS_DETAIL_MOVIE to it.imdbID)
        }
        binding.rvMovie.adapter = movieAdapter
    }
}
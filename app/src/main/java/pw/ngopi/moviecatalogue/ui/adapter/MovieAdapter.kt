package pw.ngopi.moviecatalogue.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import org.jetbrains.anko.sdk27.coroutines.onClick
import pw.ngopi.moviecatalogue.data.model.Movie
import pw.ngopi.moviecatalogue.databinding.ItemMovieBinding

class MovieAdapter(val listener: (Movie) -> Unit) :
    RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    private var list = mutableListOf<Movie>()

    fun setData(list: List<Movie>?) {
        this.list.clear()
        list?.let { this.list.addAll(it) }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    @ExperimentalStdlibApi
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (list.isNotEmpty()) {
            holder.bindItem(list[position])
        }
    }

    override fun getItemCount() = list.size

    inner class ViewHolder(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @ExperimentalStdlibApi
        fun bindItem(item: Movie) {
            Glide.with(itemView.context)
                .load(item.poster)
                .into(binding.ivMovie)
            binding.tvTitle.text = item.title
            binding.tvYear.text = item.year
            binding.root.onClick {
                listener.invoke(item)
            }
        }
    }
}
package pw.ngopi.moviecatalogue.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

data class MovieResponse(
    @Json(name = "Search")
    val search: List<Movie>?
)

@Parcelize
data class Movie(
    @Json(name = "Title")
    val title: String?,
    @Json(name = "Year")
    val year: String?,
    val imdbID: String?,
    @Json(name = "Poster")
    val poster: String?,
    @Json(name = "Genre")
    val genre: String?,
    @Json(name = "Plot")
    val plot: String?,
    val imdbRating: String?
) : Parcelable
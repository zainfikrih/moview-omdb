package pw.ngopi.moviecatalogue.data.remote

import kotlinx.coroutines.Deferred
import pw.ngopi.moviecatalogue.data.model.Movie
import pw.ngopi.moviecatalogue.data.model.MovieResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/")
    fun getMovies(
        @Query("s") search: String = "avenger",
        @Query("apikey") apiKey: String = "ca5ea82e",
        @Query("type") type: String = "movie"
    ): Deferred<MovieResponse>

    @GET("/")
    fun getMovieDetail(
        @Query("i") id: String,
        @Query("apikey") apiKey: String = "ca5ea82e"
    ): Deferred<Movie>
}
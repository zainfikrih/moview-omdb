package pw.ngopi.moviecatalogue.data

import pw.ngopi.moviecatalogue.data.remote.ApiService

class MovieRepository(
    private val apiService: ApiService
) {
    suspend fun getMovies(search: String = "avenger") = apiService.getMovies(search).await()

    suspend fun getMovieDetail(id: String) = apiService.getMovieDetail(id).await()
}
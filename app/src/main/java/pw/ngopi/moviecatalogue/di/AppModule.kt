package pw.ngopi.moviecatalogue.di

import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import pw.ngopi.moviecatalogue.data.MovieRepository
import pw.ngopi.moviecatalogue.viewmodel.MovieViewModel

val appModule = module {
    single { MovieRepository(get()) }

    viewModel { MovieViewModel(androidApplication(), get()) }
}
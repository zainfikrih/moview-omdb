package pw.ngopi.moviecatalogue.di

import org.koin.dsl.module
import pw.ngopi.moviecatalogue.data.remote.provideClient
import pw.ngopi.moviecatalogue.data.remote.provideHttpLoggingInterceptor
import pw.ngopi.moviecatalogue.data.remote.provideMoshiConverter
import pw.ngopi.moviecatalogue.data.remote.provideOkHttpClientBasic

val remoteModule = module {
    single { provideHttpLoggingInterceptor() }
    single { provideMoshiConverter() }
    single { provideOkHttpClientBasic() }
    single { provideClient(get(), "http://www.omdbapi.com/", get()) }
}
package pw.ngopi.moviecatalogue

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import pw.ngopi.moviecatalogue.di.appModule
import pw.ngopi.moviecatalogue.di.remoteModule

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@MyApp)
            modules(
                listOf(
                    remoteModule,
                    appModule
                )
            )
        }
    }
}
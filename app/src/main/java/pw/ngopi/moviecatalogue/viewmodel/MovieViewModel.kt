package pw.ngopi.moviecatalogue.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import pw.ngopi.moviecatalogue.data.MovieRepository
import pw.ngopi.moviecatalogue.data.model.Movie
import pw.ngopi.moviecatalogue.data.model.MovieResponse
import pw.ngopi.moviecatalogue.utils.Resource

class MovieViewModel(application: Application, private val movieRepository: MovieRepository) :
    AndroidViewModel(application) {

    private val _movieData = MutableLiveData<Resource<MovieResponse>>()
    val movieData: LiveData<Resource<MovieResponse>>
        get() = _movieData

    fun getMovies(search: String = "avenger") {
        _movieData.postValue(Resource.loading())
        viewModelScope.launch {
            try {
                val res = movieRepository.getMovies(search)
                _movieData.postValue(Resource.success(res))
            } catch (t: Throwable) {
                _movieData.postValue(Resource.error(t.message))
            }
        }
    }

    private val _movieDetailData = MutableLiveData<Resource<Movie>>()
    val movieDetailData: LiveData<Resource<Movie>>
        get() = _movieDetailData

    fun getMovieDetail(id: String) {
        _movieDetailData.postValue(Resource.loading())
        viewModelScope.launch {
            try {
                val res = movieRepository.getMovieDetail(id)
                _movieDetailData.postValue(Resource.success(res))
            } catch (t: Throwable) {
                _movieDetailData.postValue(Resource.error(t.message))
            }
        }
    }
}